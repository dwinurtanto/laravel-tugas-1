<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg()
    {
        return view('page.register');
    }

    public function signup(Request $request)
    {
        $namadepan = $request['firstname'];
        $namabelakang = $request['lastname'];

        return view('page.welcome', compact("namadepan", "namabelakang"));
    }
}
