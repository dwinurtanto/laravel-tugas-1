@extends('layout.master')
@section('title')
Halaman Welcome
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$namadepan}} {{$namabelakang}}</h1>
    <h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita bresama!</h3>
@endsection