@extends('layout.master')
@section('title')
Halaman Form
@endsection

@section('content')
<h4>Sign Up Form</h4>    

<form action="/signup" method="POST">
    @csrf
    <label>First Name :</label><br>
    <input type="text" name="firstname"><br><br>
    <label>Last Name :</label><br>
    <input type="text" name="lastname"><br><br>
    <label>Gender</label><br>
    <input type="radio" name="gender" value="male">Male<br>
    <input type="radio" name="gender" value="female">Female<br><br>
    <label>Nationality</label><br>
    <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="amerika">Amerika</option>
        <option value="inggris">Inggris</option>
    </select><br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox" name="language" value="id">Bahasa Indonesia<br>
    <input type="checkbox" name="language"value="en">English<br>
    <input type="checkbox" name="language" value="other">Other<br><br>
    <label>Bio</label><br>
    <textarea name="bio" rows="5" cols="30"></textarea><br><br>
    <input type="submit" value="Sign Up">
</form>
@endsection